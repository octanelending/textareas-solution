(function(){
'use strict';

var MESSAGE_TYPE = 'cross-frame-message';

/**
 * Function for posting messages to iFrame.
 */
var emit = function (otherWindow, value) {
    otherWindow.postMessage({
        value: value,
        type: MESSAGE_TYPE
    }, '*');
};

/**
 * Listen to messages from iFrame.
 */
var attachMessageListener = function (textareaSelector) {
    window.addEventListener('message', function(event) {
        if (event.data.type === MESSAGE_TYPE) {
            document.querySelector(textareaSelector).value = event.data.value;
        }
    });
};

/**
 * Handle textarea input change.
 */
var attachMessageEmitter = function (textareaSelector, otherWindow) {
    document.querySelector(textareaSelector).addEventListener(
        'input',
        function(e){ emit(otherWindow, e.currentTarget.value); }
    );
};

var attach = function(textareaSelector, otherWindow) {
    window.addEventListener('DOMContentLoaded', function(){
        attachMessageEmitter(textareaSelector,
            typeof otherWindow === 'string' ?
                document.querySelector(otherWindow).contentWindow :
                otherWindow
        );
        attachMessageListener(textareaSelector);
    });
};

window.couple = function (textareaSelector) {
    return {
        with: function(otherWindow) {
            attach(textareaSelector, otherWindow);
        }
    };
};

})();